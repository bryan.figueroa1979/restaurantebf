from django.db import models

# Create your models here.
class Provincia(models.Model):
    id_bf = models.AutoField(primary_key=True)
    nombre_bf = models.CharField(max_length=100)
    codigo_bf = models.CharField(max_length=10, blank=True, null=True)
    capital_bf = models.CharField(max_length=50, blank=True, null=True)
    region_bf = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
        fila = "{0} {1} {2} {3}"
        return fila.format(
            self.nombre_bf,
            self.codigo_bf,
            self.capital_bf,
            self.region_bf
        )

class Tipo(models.Model):
    id_bf = models.AutoField(primary_key=True)
    nombre_bf = models.CharField(max_length=100)
    descripcion_bf = models.TextField(blank=True, null=True)
    categoria_bf = models.CharField(max_length=50, blank=True, null=True)
    precio_bf = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)

    def __str__(self):
        fila = "{0} {1} {2} {3}"
        return fila.format(
            self.nombre_bf,
            self.descripcion_bf,
            self.categoria_bf,
            self.precio_bf
        )

class Ingrediente(models.Model):
    id_bf = models.AutoField(primary_key=True)
    nombre_bf = models.CharField(max_length=100)
    cantidad_disponible_bf = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    fecha_expiracion_bf = models.DateField(blank=True, null=True)
    imagen_bf = models.FileField(upload_to='ingredientes', null=True, blank=True)

    def __str__(self):
        fila = "{0} {1} {2} {3}"
        return fila.format(
            self.nombre_bf,
            self.cantidad_disponible_bf,
            self.fecha_expiracion_bf
        )

class Cliente(models.Model):
    id_bf = models.AutoField(primary_key=True)
    nombre_bf = models.CharField(max_length=150)
    direccion_bf = models.TextField(blank=True, null=True)
    telefono_bf = models.CharField(max_length=20, blank=True, null=True)
    email_bf = models.EmailField(blank=True, null=True)
    provincia = models.ForeignKey(Provincia, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        fila = "{0} {1} {2} {3}"
        return fila.format(
            self.nombre_bf,
            self.direccion_bf,
            self.telefono_bf,
            self.email_bf
        )

class Pedido(models.Model):
    id_bf = models.AutoField(primary_key=True)
    fecha_pedido_bf = models.DateField()
    tipo_servicio_bf = models.CharField(max_length=50, blank=True, null=True)
    metodo_pago_bf = models.CharField(max_length=50, blank=True, null=True)
    estado_bf = models.CharField(max_length=20, blank=True, null=True)
    cliente = models.ForeignKey(Cliente, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        fila = "{0} {1} {2} {3}"
        return fila.format(
            self.fecha_pedido_bf,
            self.tipo_servicio_bf,
            self.metodo_pago_bf,
            self.estado_bf
        )

class Platillo(models.Model):
    id_bf = models.AutoField(primary_key=True)
    nombre_bf = models.CharField(max_length=100)
    descripcion_bf = models.TextField(blank=True, null=True)
    preparacion_bf = models.TextField(blank=True, null=True)
    precio_bf = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    tipo = models.ForeignKey(Tipo, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        fila = "{0} {1} {2} {3}"
        return fila.format(
            self.nombre_bf,
            self.descripcion_bf,
            self.preparacion_bf,
            self.precio_bf
        )

class Detalle(models.Model):
    id_bf = models.AutoField(primary_key=True)
    descuento_bf = models.DecimalField(max_digits=5, decimal_places=2, default=0.0)
    impuesto_bf = models.DecimalField(max_digits=5, decimal_places=2, default=0.0)
    total_bf = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    fecha_creacion_bf = models.DateTimeField(auto_now_add=True)
    platillo = models.ForeignKey(Platillo, null=True, blank=True, on_delete=models.PROTECT)
    pedido = models.ForeignKey(Pedido, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        fila = "{0} {1} {2} {3}"
        return fila.format(
            self.descuento_bf,
            self.impuesto_bf,
            self.total_bf,
            self.fecha_creacion_bf
        )

class Receta(models.Model):
    id_bf = models.AutoField(primary_key=True)
    cantidad_necesaria_bf = models.DecimalField(max_digits=10, decimal_places=2)
    tiempo_prepraracion_bf = models.CharField(max_length=20)
    instrucciones_bf = models.TextField(blank=True, null=True)
    comentario_bf = models.TextField(blank=True, null=True)
    bafa_nombre_pais= models.CharField(max_length=100, blank=True, null=True)
    bafa_patente = models.FileField(upload_to='recetas', null=True, blank=True)
    platillo = models.ForeignKey(Platillo, null=True, blank=True, on_delete=models.PROTECT)
    ingrediente = models.ForeignKey(Ingrediente, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        fila = "{0} {1} {2} {3} {4}"
        return fila.format(
            self.cantidad_necesaria_bf,
            self.tiempo_prepraracion_bf,
            self.instrucciones_bf,
            self.comentario_bf,
            self.bafa_nombre_pais
        )
