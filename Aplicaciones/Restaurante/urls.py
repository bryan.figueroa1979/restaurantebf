from django.urls import path
from . import views

urlpatterns=[
    path('',views.listadoProvincias),
    path('guardarProvincia/',views.guardarProvincia),
    path('eliminarProvincia/<id_bf>',views.eliminarProvincia),
    path('editarProvincia/<id_bf>',views.editarProvincia),
    path('procesarActualizacionProvincia/',views.procesarActualizacionProvincia),
#URLS TIPO
    path('tipos/', views.listadoTipos, name='tipos'),
    path('guardarTipo/',views.guardarTipo),
    path('eliminarTipo/<id_bf>',views.eliminarTipo),
    path('editarTipo/<id_bf>',views.editarTipo),
    path('procesarActualizacionTipo/',views.procesarActualizacionTipo),
#URLS INGREDIENTES
    path('ingredientes/', views.listadoIngredientes, name='ingredientes'),
    path('guardarIngrediente/',views.guardarIngrediente),
    path('eliminarIngrediente/<id_bf>',views.eliminarIngrediente),
    path('editarIngrediente/<id_bf>',views.editarIngrediente),
    path('procesarActualizacionIngrediente/',views.procesarActualizacionIngrediente),
#URLS DE CLIENTES
    path('clientes/', views.listadoClientes, name='clientes'),
    path('guardarCliente/',views.guardarCliente),
    path('eliminarCliente/<id_bf>',views.eliminarCliente),
    path('editarCliente/<id_bf>',views.editarCliente),
    path('procesarActualizacionCliente/',views.procesarActualizacionCliente),
#URLS PEDIDOS
    path('pedidos/', views.listadoPedidos, name='pedidos'),
    path('guardarPedido/',views.guardarPedido),
    path('eliminarPedido/<id_bf>',views.eliminarPedido),
    path('editarPedido/<id_bf>',views.editarPedido),
    path('procesarActualizacionPedido/',views.procesarActualizacionPedido),
#URLS TIPO PLATILLOS
    path('platillos/', views.listadoPlatillos, name='platillos'),
    path('guardarPlatillo/',views.guardarPlatillo),
    path('eliminarPlatillo/<id_bf>',views.eliminarPlatillo),
    path('editarPlatillo/<id_bf>',views.editarPlatillo),
    path('procesarActualizacionPlatillo/',views.procesarActualizacionPlatillo),
#URLS TIPO DE DETALLES
    path('detalles/', views.listadoDetalles, name='detalles'),
    path('guardarDetalle/',views.guardarDetalle),
    path('eliminarDetalle/<id_bf>',views.eliminarDetalle),
    path('editarDetalle/<id_bf>',views.editarDetalle),
    path('procesarActualizacionDetalle/',views.procesarActualizacionDetalle),
#URLS TIPO DE RECETAS
    path('recetas/', views.listadoRecetas, name='recetas'),
    path('guardarReceta/',views.guardarReceta),
    path('eliminarReceta/<id_bf>',views.eliminarReceta),
    path('editarReceta/<id_bf>',views.editarReceta),
    path('procesarActualizacionReceta/',views.procesarActualizacionReceta)

]
