from django.contrib import admin
from .models import Provincia
from .models import Cliente
from .models import Platillo
from .models import Detalle
from .models import Receta
from .models import Tipo
from .models import Ingrediente
from .models import Pedido

# Register your models here.
admin.site.register(Provincia)
admin.site.register(Cliente)
admin.site.register(Platillo)
admin.site.register(Detalle)
admin.site.register(Receta)
admin.site.register(Tipo)
admin.site.register(Ingrediente)
admin.site.register(Pedido)
