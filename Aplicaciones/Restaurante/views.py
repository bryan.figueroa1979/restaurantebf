from django.shortcuts import render, redirect
from .models import Provincia
from .models import Cliente
from .models import Platillo
from .models import Detalle
from .models import Receta
from .models import Tipo
from .models import Ingrediente
from .models import Pedido
from django.contrib import messages

# Create your views here.
def listadoProvincias(request):
    provinciasbdd=Provincia.objects.all()
    return render(request, 'listadoProvincias.html',{'provincias' : provinciasbdd})

def guardarProvincia(request):
    #Capturando los valores del formulario por POST
    nombre_bf=request.POST["nombre_bf"]
    codigo_bf=request.POST["codigo_bf"]
    capital_bf=request.POST["capital_bf"]
    region_bf=request.POST["region_bf"]
    #Insertando daots metiante el ORM del framework Djangocedula
    nuevoProvincia=Provincia.objects.create(
        nombre_bf=nombre_bf,
        codigo_bf=codigo_bf,
        capital_bf=capital_bf,
        region_bf=region_bf
    )
    messages.success(request,'Provincias Guardadas Correctamente')
    return redirect('/')

def eliminarProvincia(request, id_bf):
    provinciaEliminar = Provincia.objects.get(id_bf=id_bf)
    provinciaEliminar.delete()
    messages.error(request, 'Provincia Eliminado Exitosamente')
    return redirect('/')

def editarProvincia(request, id_bf):
    provinciaEditar = Provincia.objects.get(id_bf=id_bf)
    return render(request, 'editarProvincia.html', {'provincia': provinciaEditar})

def procesarActualizacionProvincia(request):
    id_bf=request.POST["id_bf"]
    nombre_bf=request.POST["nombre_bf"]
    codigo_bf=request.POST["codigo_bf"]
    capital_bf=request.POST["capital_bf"]
    region_bf=request.POST["region_bf"]

    provinciaEditar = Provincia.objects.get(id_bf=id_bf)
    provinciaEditar.nombre_bf = nombre_bf
    provinciaEditar.codigo_bf = codigo_bf
    provinciaEditar.capital_bf = capital_bf
    provinciaEditar.region_bf = region_bf
    provinciaEditar.save()

    messages.warning(request, 'Provincia Actualizada Correctamente')
    return redirect('/')

# TABLA DE TIPO
def listadoTipos(request):
    tiposbdd=Tipo.objects.all()
    return render(request, 'listadoTipos.html',{'tipos' : tiposbdd})

def guardarTipo(request):
    #Capturando los valores del formulario por POST
    nombre_bf=request.POST["nombre_bf"]
    descripcion_bf=request.POST["descripcion_bf"]
    categoria_bf=request.POST["categoria_bf"]
    precio_bf=request.POST["precio_bf"].replace(',', '.')
    #Insertando daots metiante el ORM del framework Djangocedula
    nuevoTipo=Tipo.objects.create(
        nombre_bf=nombre_bf,
        descripcion_bf=descripcion_bf,
        categoria_bf=categoria_bf,
        precio_bf=precio_bf
    )
    messages.success(request, 'Tipo de platillo Guardado Correctamente')
    return redirect('/tipos')

def eliminarTipo(request, id_bf):
    tipoEliminar = Tipo.objects.get(id_bf=id_bf)
    tipoEliminar.delete()
    messages.error(request, 'Tipo de Platillo Eliminado Exitosamente')
    return redirect('/tipos/')

def editarTipo(request, id_bf):
    tipoEditar = Tipo.objects.get(id_bf=id_bf)
    return render(request, 'editarTipo.html', {'tipo': tipoEditar})

def procesarActualizacionTipo(request):
    id_bf=request.POST["id_bf"]
    nombre_bf=request.POST["nombre_bf"]
    descripcion_bf=request.POST["descripcion_bf"]
    categoria_bf=request.POST["categoria_bf"]
    precio_bf=request.POST["precio_bf"].replace(',', '.')

    tipoEditar = Tipo.objects.get(id_bf=id_bf)
    tipoEditar.nombre_bf = nombre_bf
    tipoEditar.descripcion_bf = descripcion_bf
    tipoEditar.categoria_bf = categoria_bf
    tipoEditar.precio_bf = precio_bf
    tipoEditar.save()

    messages.warning(request, 'Tipo de Platillo Actualizado Correctamente')
    return redirect('/tipos/')

# TABLA DE Cliente
def listadoClientes(request):
    clientesbdd=Cliente.objects.all()
    provinciasbdd=Provincia.objects.all()
    return render(request, 'listadoClientes.html',{'clientes' : clientesbdd, 'provincias': provinciasbdd})

def guardarCliente(request):
    #Capturando los valores del formulario por Post
    id_prov=request.POST["id_prov"]
    provinciaSeleccionada=Provincia.objects.get(id_bf=id_prov)
    #Capturando los valores del formulario por POST
    nombre_bf=request.POST["nombre_bf"]
    direccion_bf=request.POST["direccion_bf"]
    telefono_bf=request.POST["telefono_bf"]
    email_bf=request.POST["email_bf"]
    #Insertando daots metiante el ORM del framework Djangocedula
    nuevoCliente=Cliente.objects.create(
        nombre_bf=nombre_bf,
        direccion_bf=direccion_bf,
        telefono_bf=telefono_bf,
        email_bf=email_bf,
        provincia=provinciaSeleccionada
    )
    messages.success(request, 'Cliente Guardado Correctamente')
    return redirect('/clientes/')

def eliminarCliente(request, id_bf):
    clienteEliminar = Cliente.objects.get(id_bf=id_bf)
    clienteEliminar.delete()
    messages.error(request, 'Cliente Eliminado Exitosamente')
    return redirect('/clientes/')

def editarCliente(request, id_bf):
    clienteEditar = Cliente.objects.get(id_bf=id_bf)
    provinciasbdd = Provincia.objects.all()
    return render(request, 'editarCliente.html', {'cliente': clienteEditar, 'provincias': provinciasbdd})

def procesarActualizacionCliente(request):
    id_bf=request.POST["id_bf"]
    id_prov=request.POST["id_prov"]
    provinciaSeleccionada=Provincia.objects.get(id_bf=id_prov)
    #Capturando los valores del formulario por POST
    nombre_bf=request.POST["nombre_bf"]
    direccion_bf=request.POST["direccion_bf"]
    telefono_bf=request.POST["telefono_bf"]
    email_bf=request.POST["email_bf"]

    clienteEditar = Cliente.objects.get(id_bf=id_bf)
    clienteEditar.provincia = provinciaSeleccionada
    clienteEditar.nombre_bf = nombre_bf
    clienteEditar.direccion_bf = direccion_bf
    clienteEditar.telefono_bf = telefono_bf
    clienteEditar.email_bf = email_bf
    clienteEditar.save()

    messages.warning(request, 'Cliente Actualizado Correctamente')
    return redirect('/clientes/')

# TABLA DE INGREDIENTES
def listadoIngredientes(request):
    ingredientesbdd=Ingrediente.objects.all()
    return render(request, 'listadoIngredientes.html',{'ingredientes' : ingredientesbdd})

def guardarIngrediente(request):
    #Capturando los valores del formulario por POST
    nombre_bf=request.POST["nombre_bf"]
    cantidad_disponible_bf=request.POST["cantidad_disponible_bf"].replace(',', '.')
    fecha_expiracion_bf=request.POST["fecha_expiracion_bf"]
    imagen_bf = request.FILES.get("imagen_bf")
    #Insertando daots metiante el ORM del framework Djangocedula
    nuevoIngrediene=Ingrediente.objects.create(
        nombre_bf=nombre_bf,
        cantidad_disponible_bf=cantidad_disponible_bf,
        fecha_expiracion_bf=fecha_expiracion_bf,
        imagen_bf=imagen_bf
    )
    messages.success(request, 'Ingrediente Guardado Correctamente')
    return redirect('/ingredientes/')

def eliminarIngrediente(request, id_bf):
    ingredienteEliminar = Ingrediente.objects.get(id_bf=id_bf)
    ingredienteEliminar.delete()
    messages.error(request, 'Ingrediente Eliminado Exitosamente')
    return redirect('/ingredientes/')

def editarIngrediente(request, id_bf):
    ingredienteEditar = Ingrediente.objects.get(id_bf=id_bf)
    return render(request, 'editarIngrediente.html', {'ingrediente': ingredienteEditar})

def procesarActualizacionIngrediente(request):
    id_bf=request.POST["id_bf"]
    nombre_bf=request.POST["nombre_bf"]
    cantidad_disponible_bf=request.POST["cantidad_disponible_bf"].replace(',', '.')
    fecha_expiracion_bf=request.POST["fecha_expiracion_bf"]
    imagen_bf = request.FILES.get("imagen_bf")

    ingredienteEditar = Ingrediente.objects.get(id_bf=id_bf)
    ingredienteEditar.nombre_bf = nombre_bf
    ingredienteEditar.cantidad_disponible_bf = cantidad_disponible_bf
    ingredienteEditar.fecha_expiracion_bf = fecha_expiracion_bf
    ingredienteEditar.imagen_bf = imagen_bf
    ingredienteEditar.save()

    messages.warning(request, 'Ingrediente Actualizado Correctamente')
    return redirect('/ingredientes/')

# TABLA DE PEDIDO
def listadoPedidos(request):
    pedidosbdd=Pedido.objects.all()
    clientesbdd=Cliente.objects.all()
    return render(request, 'listadoPedidos.html',{'pedidos' : pedidosbdd, 'clientes': clientesbdd})

def guardarPedido(request):
    #Capturando los valores del formulario por Post
    id_cli=request.POST["id_cli"]
    clienteSeleccionado=Cliente.objects.get(id_bf=id_cli)
    #Capturando los valores del formulario por POST
    fecha_pedido_bf=request.POST["fecha_pedido_bf"]
    tipo_servicio_bf=request.POST["tipo_servicio_bf"]
    metodo_pago_bf=request.POST["metodo_pago_bf"]
    estado_bf=request.POST["estado_bf"]
    #Insertando daots metiante el ORM del framework Djangocedula
    nuevoPedido=Pedido.objects.create(
        fecha_pedido_bf=fecha_pedido_bf,
        tipo_servicio_bf=tipo_servicio_bf,
        metodo_pago_bf=metodo_pago_bf,
        estado_bf=estado_bf,
        cliente=clienteSeleccionado
    )
    messages.success(request, 'Pedido Guardado Correctamente')
    return redirect('/pedidos/')

def eliminarPedido(request, id_bf):
    pedidoEliminar = Pedido.objects.get(id_bf=id_bf)
    pedidoEliminar.delete()
    messages.error(request, 'Pedido Eliminado Exitosamente')
    return redirect('/pedidos/')

def editarPedido(request, id_bf):
    pedidoEditar = Pedido.objects.get(id_bf=id_bf)
    clientesbdd = Cliente.objects.all()
    return render(request, 'editarPedido.html', {'pedido': pedidoEditar, 'clientes': clientesbdd})

def procesarActualizacionPedido(request):
    id_bf=request.POST["id_bf"]
    id_cli=request.POST["id_cli"]
    clienteSeleccionado=Cliente.objects.get(id_bf=id_cli)
    #Capturando los valores del formulario por POST
    fecha_pedido_bf=request.POST["fecha_pedido_bf"]
    tipo_servicio_bf=request.POST["tipo_servicio_bf"]
    metodo_pago_bf=request.POST["metodo_pago_bf"]
    estado_bf=request.POST["estado_bf"]

    pedidoEditar = Pedido.objects.get(id_bf=id_bf)
    pedidoEditar.cliente = clienteSeleccionado
    pedidoEditar.fecha_pedido_bf = fecha_pedido_bf
    pedidoEditar.tipo_servicio_bf = tipo_servicio_bf
    pedidoEditar.metodo_pago_bf = metodo_pago_bf
    pedidoEditar.estado_bf = estado_bf
    pedidoEditar.save()

    messages.warning(request, 'Pedido Actualizado Correctamente')
    return redirect('/pedidos/')

# TABLA DE PLATILLO
def listadoPlatillos(request):
    platillosbdd=Platillo.objects.all()
    tiposbdd=Tipo.objects.all()
    return render(request, 'listadoPlatillos.html',{'platillos' : platillosbdd, 'tipos': tiposbdd})

def guardarPlatillo(request):
    #Capturando los valores del formulario por Post
    id_tip=request.POST["id_tip"]
    tipoSeleccionado=Tipo.objects.get(id_bf=id_tip)
    #Capturando los valores del formulario por POST
    nombre_bf=request.POST["nombre_bf"]
    descripcion_bf=request.POST["descripcion_bf"]
    preparacion_bf=request.POST["preparacion_bf"]
    precio_bf=request.POST["precio_bf"].replace(',', '.')
    #Insertando daots metiante el ORM del framework Djangocedula
    nuevoPlatillo=Platillo.objects.create(
        nombre_bf=nombre_bf,
        descripcion_bf=descripcion_bf,
        preparacion_bf=preparacion_bf,
        precio_bf=precio_bf,
        tipo=tipoSeleccionado
    )
    messages.success(request, 'Platillo Guardado Correctamente')
    return redirect('/platillos/')

def eliminarPlatillo(request, id_bf):
    platilloEliminar = Platillo.objects.get(id_bf=id_bf)
    platilloEliminar.delete()
    messages.error(request, 'Platillo Eliminado Exitosamente')
    return redirect('/platillos/')

def editarPlatillo(request, id_bf):
    platilloEditar = Platillo.objects.get(id_bf=id_bf)
    tiposbdd = Tipo.objects.all()
    return render(request, 'editarPlatillo.html', {'platillo': platilloEditar, 'tipos': tiposbdd})

def procesarActualizacionPlatillo(request):
    id_bf=request.POST["id_bf"]
    id_tip=request.POST["id_tip"]
    tipoSeleccionado=Tipo.objects.get(id_bf=id_tip)
    #Capturando los valores del formulario por POST
    nombre_bf=request.POST["nombre_bf"]
    descripcion_bf=request.POST["descripcion_bf"]
    preparacion_bf=request.POST["preparacion_bf"]
    precio_bf=request.POST["precio_bf"].replace(',', '.')

    platilloEditar = Platillo.objects.get(id_bf=id_bf)
    platilloEditar.tipo = tipoSeleccionado
    platilloEditar.nombre_bf = nombre_bf
    platilloEditar.descripcion_bf = descripcion_bf
    platilloEditar.preparacion_bf = preparacion_bf
    platilloEditar.precio_bf = precio_bf
    platilloEditar.save()

    messages.warning(request, 'Platillo Actualizado Correctamente')
    return redirect('/platillos/')

# TABLA DE DETALLES
def listadoDetalles(request):
    detallesbdd = Detalle.objects.all()
    platillosbdd=Platillo.objects.all()
    pedidosbdd=Pedido.objects.all()
    return render(request, 'listadoDetalles.html',{'detalles' : detallesbdd, 'platillos': platillosbdd, 'pedidos': pedidosbdd})

def guardarDetalle(request):
    #Capturando los valores del formulario por Post
    id_plt=request.POST["id_plt"]
    id_pd=request.POST["id_pd"]
    platilloSeleccionado=Platillo.objects.get(id_bf=id_plt)
    pedidoSeleccionado=Pedido.objects.get(id_bf=id_pd)
    #Capturando los valores del formulario por POST
    descuento_bf=request.POST["descuento_bf"]
    impuesto_bf=request.POST["impuesto_bf"]
    total_bf=request.POST["total_bf"].replace(',', '.')
    fecha_creacion_bf=request.POST["fecha_creacion_bf"]
    #Insertando daots metiante el ORM del framework Djangocedula
    nuevoDetalle=Detalle.objects.create(
        descuento_bf=descuento_bf,
        impuesto_bf=impuesto_bf,
        total_bf=total_bf,
        fecha_creacion_bf=fecha_creacion_bf,
        platillo=platilloSeleccionado,
        pedido=pedidoSeleccionado
    )
    messages.success(request, 'Detalle Guardado Correctamente')
    return redirect('/detalles/')

def eliminarDetalle(request, id_bf):
    detalleEliminar = Detalle.objects.get(id_bf=id_bf)
    detalleEliminar.delete()
    messages.error(request, 'Detalle Eliminado Exitosamente')
    return redirect('/detalles/')

def editarDetalle(request, id_bf):
    detalleEditar = Detalle.objects.get(id_bf=id_bf)
    platillosbdd=Platillo.objects.all()
    pedidosbdd=Pedido.objects.all()
    return render(request, 'editarDetalle.html', {'detalle': detalleEditar, 'platillos': platillosbdd, 'pedidos': pedidosbdd})

def procesarActualizacionDetalle(request):
    id_bf=request.POST["id_bf"]
    id_plt=request.POST["id_plt"]
    id_pd=request.POST["id_pd"]
    platilloSeleccionado=Platillo.objects.get(id_bf=id_plt)
    pedidoSeleccionado=Pedido.objects.get(id_bf=id_pd)
    #Capturando los valores del formulario por POST
    descuento_bf = request.POST["descuento_bf"].replace(',', '.')
    impuesto_bf = request.POST["impuesto_bf"].replace(',', '.')
    total_bf = request.POST["total_bf"].replace(',', '.')
    fecha_creacion_bf=request.POST["fecha_creacion_bf"]

    detalleEditar = Detalle.objects.get(id_bf=id_bf)
    detalleEditar.platillo = platilloSeleccionado
    detalleEditar.pedido = pedidoSeleccionado
    detalleEditar.descuento_bf = descuento_bf
    detalleEditar.impuesto_bf = impuesto_bf
    detalleEditar.total_bf = total_bf
    detalleEditar.fecha_creacion_bf = fecha_creacion_bf
    detalleEditar.save()

    messages.warning(request, 'Detalle Actualizado Correctamente')
    return redirect('/detalles/')

# TABLA DE RECETAS
def listadoRecetas(request):
    recetasbdd = Receta.objects.all()
    platillosbdd=Platillo.objects.all()
    ingredientesbdd=Ingrediente.objects.all()
    return render(request, 'listadoRecetas.html',{'recetas' : recetasbdd, 'platillos': platillosbdd, 'ingredientes': ingredientesbdd})

def guardarReceta(request):
    #Capturando los valores del formulario por Post
    id_pla=request.POST["id_pla"]
    id_ing=request.POST["id_ing"]
    platilloSelec=Platillo.objects.get(id_bf=id_pla)
    ingredienteSeleccionado=Ingrediente.objects.get(id_bf=id_ing)
    #Capturando los valores del formulario por POST
    cantidad_necesaria_bf=request.POST["cantidad_necesaria_bf"]
    tiempo_prepraracion_bf=request.POST["tiempo_prepraracion_bf"]
    instrucciones_bf=request.POST["instrucciones_bf"]
    comentario_bf=request.POST["comentario_bf"]
    comentario_bf=request.POST["comentario_bf"]
    comentario_bf=request.POST["comentario_bf"]
    bafa_nombre_pais=request.POST["bafa_nombre_pais"]
    bafa_patente = request.FILES.get("bafa_patente")
    #Insertando daots metiante el ORM del framework Djangocedula
    nuevoReceta=Receta.objects.create(
        cantidad_necesaria_bf=cantidad_necesaria_bf,
        tiempo_prepraracion_bf=tiempo_prepraracion_bf,
        instrucciones_bf=instrucciones_bf,
        comentario_bf=comentario_bf,
        platillo=platilloSelec,
        ingrediente=ingredienteSeleccionado,
        bafa_nombre_pais = bafa_nombre_pais,
        bafa_patente = bafa_patente
    )
    messages.success(request, 'Receta Guardado Correctamente')
    return redirect('/recetas/')

def eliminarReceta(request, id_bf):
    recetaEliminar = Receta.objects.get(id_bf=id_bf)
    recetaEliminar.delete()
    messages.error(request, 'Receta Eliminado Exitosamente')
    return redirect('/recetas/')

def editarReceta(request, id_bf):
    recetaEditar = Receta.objects.get(id_bf=id_bf)
    platillosbdd=Platillo.objects.all()
    ingredientesbdd=Ingrediente.objects.all()
    return render(request, 'editarReceta.html', {'receta': recetaEditar, 'platillos': platillosbdd, 'ingredientes': ingredientesbdd})

def procesarActualizacionReceta(request):
    id_bf=request.POST["id_bf"]
    id_pla=request.POST["id_pla"]
    id_ing=request.POST["id_ing"]
    platilloSelec=Platillo.objects.get(id_bf=id_pla)
    ingredienteSeleccionado=Ingrediente.objects.get(id_bf=id_ing)
    #Capturando los valores del formulario por POST
    cantidad_necesaria_bf=request.POST["cantidad_necesaria_bf"].replace(',', '.')
    tiempo_prepraracion_bf=request.POST["tiempo_prepraracion_bf"]
    instrucciones_bf=request.POST["instrucciones_bf"]
    comentario_bf=request.POST["comentario_bf"]
    bafa_nombre_pais=request.POST["bafa_nombre_pais"]
    bafa_patente = request.FILES.get("bafa_patente")

    recetaEditar = Receta.objects.get(id_bf=id_bf)
    recetaEditar.platillo = platilloSelec
    recetaEditar.ingrediente = ingredienteSeleccionado
    recetaEditar.cantidad_necesaria_bf = cantidad_necesaria_bf
    recetaEditar.tiempo_prepraracion_bf = tiempo_prepraracion_bf
    recetaEditar.instrucciones_bf = instrucciones_bf
    recetaEditar.comentario_bf = comentario_bf
    recetaEditar.bafa_nombre_pais = bafa_nombre_pais
    recetaEditar.bafa_patente = bafa_patente
    recetaEditar.save()

    messages.warning(request, 'Receta Actualizado Correctamente')
    return redirect('/recetas/')
